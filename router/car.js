const express = require('express') 
const router = express.Router()
const { list, create, update, destroy, findById} = require('../controllers/carController.js')
const validate = require('../middleware/validate')
const { createCarRules } = require('../validators/rule')
const checkToken = require('../middleware/checkToken')


router.get('/list',checkToken, list)
router.post('/find-by-id', checkToken, findById)
router.post('/create', checkToken, validate(createCarRules), create)
router.put('/update', checkToken, update) 
router.delete('/destroy', checkToken, destroy) 

module.exports = router 