const model = require('../models'), 
    service = require('../services/carService')

module.exports = {
    list: async (req, res) => {
        try {
            const datas = await service.getAll()

            return res.status(200).json({
                "success" : true,
                "error" : 0,
                "message" : "data successfully listed",
                "data" : datas
            })
        } catch (error) {
            return res.status(500).json({
                "success" : false,
                "error" : error.code,
                "message" : error,
                "data" : null
            })
        }
    },
    findById: async (req, res) => {
        try {
            const datas = await service.findById(req.body.id)

            return res.status(200).json({
                "success" : true,
                "error" : 0,
                "message" : "data successfully listed",
                "data" : datas
            })
        } catch (error) {
            return res.status(500).json({
                "success" : false,
                "error" : error.code,
                "message" : error,
                "data" : null
            })
        }
    },
    create: async(req, res) => {
        try {
            const data = await model.car.create(req.body)

            return res.status(200).json({
                "success" : true,
                "error" : 0,
                "message" : "data successfully created",
                "data" : data
            })
        } catch (error) {
            return res.status(500).json({
                "success" : false,
                "error" : error.code,
                "message" : error,
                "data" : null
            })
        }
    },
    update: async(req, res) => {
        try {
            const data = await model.car.update({
                name: req.body.name,
                type: req.body.type,
                price: req.body.price,
                path: req.body.path
            }, {
                where: {
                    id: req.body.id
                }
            })

            return res.status(200).json({
                "success" : true,
                "error" : 0,
                "message" : "data successfully updated",
                "data" : data
            })
        } catch (error) {
            return res.status(500).json({
                "success" : false,
                "error" : error.code,
                "message" : error,
                "data" : null
            })
        }
    },
    destroy: async(req, res) => {
        try {
            const data = await model.car.destroy({
                where: {
                    id: req.body.id
                }
            })

            return res.status(200).json({
                "success" : true,
                "error" : 0,
                "message" : "data successfully deleted",
                "data" : data
            })
        } catch (error) {
            return res.status(500).json({
                "success" : false,
                "error" : error.code,
                "message" : error,
                "data" : null
            })
        }
    }
}